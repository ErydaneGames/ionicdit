/**
* @file jq.utils.js
*/

/** 
* Loads a node view and insert it's contents to the index.html
* 
* @see http://stackoverflow.com/questions/14653202/jquery-load-content-sequentially
* @param nodeID {string}: The node's number
*/

function viewLoad(nodeID) {
   
   $('#content').load('../views/node-' + nodeID + '.html #node-' + nodeID);
}