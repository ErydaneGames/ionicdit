/**
* @file jq.vars.js
*/

// Global variables

var vars = {
  
  game: {
  	lang: 'en',
  	langIso: 'eng',
  	volume: false,
  	difficulty: 'easy',
  	enabled: false
  },

  skin: {
  	one: true,
  	two: false,
  	three: false,
  	four: false,
  	five: false
  },

  player: {
  	name: null,
  	ranking: null,
  	lives: 0,
  	stamina: 0,
  	globalScore: null,
  	stats: {
  		currentLevel: 0,
  		currentStage: 0,
  		inTouchTime: 0,
  		timesDied: 0,
  		timePlayed: 0,
  		bonusGot: 0,
  		favouriteSkin: 0
  	}
  }
}
